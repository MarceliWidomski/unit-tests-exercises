/*
 * testingGCD.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (TestingGCD, ZeroZero){
	EXPECT_EQ(0, greatestCommonDivisor(0,0));
}
TEST (TestingGCD, NegativePositive){
	EXPECT_EQ(2, greatestCommonDivisor(-4,2));
}
TEST (TestingGCD, PositivePositive){
	EXPECT_EQ(2, greatestCommonDivisor(4,2));
}
TEST (TestingGCD, NegtiveNegative){
	EXPECT_EQ(3, greatestCommonDivisor(-6,-3));
}
TEST (TestingGCD, PositiveNegative){
	EXPECT_EQ(3, greatestCommonDivisor(6,-3));
}
TEST (TestingGCD, PositiveZero){
	EXPECT_EQ(6, greatestCommonDivisor(6,0));
}
TEST (TestingGCD, NegativeZero){
	EXPECT_EQ(6, greatestCommonDivisor(-6,0));
}
