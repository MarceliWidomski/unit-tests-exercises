/*
 * testingDaysOfMonth.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (TestingDaysOfMonth, FebDefault){
	EXPECT_TRUE(daysOfMonth(2) == 28);
}
TEST (TestingDaysOfMonth, FebLeap){
	EXPECT_TRUE(daysOfMonth(2, 4) == 29);
}
TEST (TestingDaysOfMonth, FebZero){
	EXPECT_FALSE(daysOfMonth(2, 0));
}
TEST (TestingDaysOfMonth, FebNegativeLeap){
	EXPECT_TRUE(daysOfMonth(2, -1992) == 29);
}
TEST (TestingDaysOfMonth, NegativeDefault){
	EXPECT_FALSE(daysOfMonth(-3));
}
TEST (TestingDaysOfMonth, InvalidDefault){
	EXPECT_FALSE(daysOfMonth(13));
}
