/*
 * functions.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#include "functions.h"
#include <iostream>
#include <cstdlib>

int factorial(int number) {
	int result(1);
	if (number < 0 || number > 12)
		return 0;
	if (number == 0) {
		return result;
	}
	result = number * factorial(number - 1);
}
int fibonacci(int n) {
	if (n <= 0 || n > 46) {
		return 0;
	}
	if (n == 1) {
		return 1;
	}
	return fibonacci(n - 1) + fibonacci(n - 2);
}
int greatestCommonDivisor(int k, int n) {
	k = abs(k);
	n = abs(n);
	if (k == 0) {
		return n;
	}
	return greatestCommonDivisor(n % k, k);
}
int daysOfMonth(int month, int year) {
	if (month < 1 || month > 12 || year == 0)
		return 0;
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		return 31;
	case 4:
	case 6:
	case 9:
	case 11:
		return 30;
	case 2: {
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
			return 29;
		else
			return 28;
		default:
		return 0;
	}
	}
}
bool isDateValid(int day, int month, int year) {
	bool isDateValid(1);
	bool isLeap(0);
	if (year == 0)
		isDateValid = 0;
	else if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) // checks if year is leap year
		isLeap = 1;
	if (day < 1 || day > 31 || month < 1 || month > 12)
		isDateValid = 0;
	else if (day == 31
			&& (month == 4 || month == 6 || month == 9 || month == 11))
		isDateValid = 0;
	else if (month == 2 && (day > 29 || (isLeap == 0 && day > 28)))
		isDateValid = 0;
	return isDateValid;
}
int numberOfDigits(int number) {
	int temp(number);
	int numberOfDigits(0);
	if (temp == 0)
		++numberOfDigits;
	while (temp != 0) {
		++numberOfDigits;
		temp /= 10;
	}
	return numberOfDigits;
}
int roundExamResult(int examResult) {
	if (examResult < 0 || examResult > 100)
		return -1;
	else if (examResult > 40) {
		int temp = ((examResult / 5) + 1) * 5;
		if ((temp - examResult) <= 2)
			return temp;
		else
			return examResult;
	} else
		return examResult;
}
int productOfDigits(int number) {
	if (number) {
		int result(1);
		while (number != 0) {
			int lastDigit(number % 10);
			number /= 10;
			result *= lastDigit;
		}
		return result;
	} else
		return 0;
}
std::string changeNumberToString(int number) {
	if (!number)
		return "zero ";
	std::string output;
	bool isNegative(0);
	if (number < 0) {
		number = -number;
		isNegative = 1;
	}
	while (number > 0) {
		int lastDigit = number % 10;
		number /= 10;
		switch (lastDigit) {
		case 1:
			output = "one " + output;
			break;
		case 2:
			output = "two " + output;
			break;
		case 3:
			output = "three " + output;
			break;
		case 4:
			output = "four " + output;
			break;
		case 5:
			output = "five " + output;
			break;
		case 6:
			output = "six " + output;
			break;
		case 7:
			output = "seven " + output;
			break;
		case 8:
			output = "eight " + output;
			break;
		case 9:
			output = "nine " + output;
			break;
		case 0:
			output = "zero " + output;
			break;
		}
	}
	if (isNegative)
		output = "minus " + output;
	return output;
}
