/*
 * testingRoundExamResult.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"

TEST (TestingRoundExamResult, Negative){
	EXPECT_EQ(-1, roundExamResult(-5));
}
TEST (TestingRoundExamResult, over100){
	EXPECT_EQ(-1, roundExamResult(101));
}
TEST (TestingRoundExamResult, under40){
	EXPECT_EQ(34, roundExamResult(34));
}
TEST (TestingRoundExamResult, examResult43){
	EXPECT_EQ(45, roundExamResult(43));
}
TEST (TestingRoundExamResult, examResult44){
	EXPECT_EQ(45, roundExamResult(44));
}
TEST (TestingRoundExamResult, examResult99){
	EXPECT_EQ(100, roundExamResult(99));
}
