/*
 * testingNumberOfDigits.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "functions.h"


TEST (TestingNumberOfDigits, HandlesZero){
	EXPECT_EQ(1, numberOfDigits(0));
}
TEST (TestingNumberOfDigits, HandlesNegative){
	EXPECT_EQ(1, numberOfDigits(-1));
	EXPECT_EQ(2, numberOfDigits(-11));
	EXPECT_EQ(3, numberOfDigits(-111));
}
TEST (TestingNumberOfDigits, HandlesPositive){
	EXPECT_EQ(1, numberOfDigits(1));
	EXPECT_EQ(2, numberOfDigits(10));
	EXPECT_EQ(3, numberOfDigits(101));
}
