/*
 * testingFibonacci.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (TestingFibonacci, zeroElement){
	EXPECT_EQ (0, fibonacci(0));
}
TEST (TestingFibonacci, firstElement){
	EXPECT_EQ (1, fibonacci(1));
}
TEST (TestingFibonacci, secondElement){
	EXPECT_EQ(1, fibonacci(2));
}
TEST (TestingFibonacci, thirdElement){
	EXPECT_EQ(2, fibonacci(3));
}
TEST (TestingFibonacci, twelfthElement){
	EXPECT_EQ(144, fibonacci(12));
}
TEST (TestingFibonacci, OutOfIntRange){
	EXPECT_EQ(0, fibonacci(47));
}
TEST (TestingFibonacci, Negative){
	EXPECT_EQ(0, fibonacci(-5));
}
