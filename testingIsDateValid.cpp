/*
 * testingIsDateValid.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (testingIsDateValid, ZeroZeroZero){
	EXPECT_FALSE(isDateValid(0, 0, 0));
}
TEST (testingIsDateValid, ValidZeroZero){
	EXPECT_FALSE(isDateValid(1, 0, 0));
}
TEST (testingIsDateValid, ValidValidZero){
	EXPECT_FALSE(isDateValid(1, 1, 0));
}
TEST (testingIsDateValid, ZeroValidZero){
	EXPECT_FALSE(isDateValid(0, 1, 0));
}
TEST (testingIsDateValid, ZeroZeroValid){
	EXPECT_FALSE(isDateValid(0, 0, 1));
}
TEST (testingIsDateValid, ValidValidValid){
	EXPECT_TRUE(isDateValid(1, 1, 1));
}
TEST (testingIsDateValid, ValidFebLeap){
	EXPECT_TRUE(isDateValid(29, 2, 2016));
}
TEST (testingIsDateValid, ValidFebNotLeap){
	EXPECT_FALSE(isDateValid(29, 2, 2015));
}
TEST (testingIsDateValid, NegativeValidValid){
	EXPECT_FALSE(isDateValid(-10, 6, 2015));
}
TEST (testingIsDateValid, ValidNegativeValid){
	EXPECT_FALSE(isDateValid(1, -29 , 2015));
}
TEST (testingIsDateValid, ValidValidNegative){
	EXPECT_TRUE(isDateValid(1, 6, -12));
}
TEST (testingIsDateValid, lastDayOfYear){
	EXPECT_TRUE(isDateValid(31, 12, 2017));
}
TEST (testingIsDateValid, firstDayOfYear){
	EXPECT_TRUE(isDateValid(1, 1, 2017));
}

