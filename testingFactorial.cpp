/*
 * testingFactorial.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (TestingFactorial, Factorial0){
	EXPECT_EQ (1, factorial(0));
}
TEST (TestingFactorial, Factorial1){
	EXPECT_EQ (1, factorial(1));
}
TEST (TestingFactorial, Factorial2){
	EXPECT_EQ (2, factorial(2));
}
TEST (TestingFactorial, Factorial12){
	EXPECT_EQ (479001600, factorial(12));
}
TEST (TestingFactorial, Factorial13){
	EXPECT_EQ(0, factorial(13));
}
TEST (TestingFactorial, FactorialNegative){
	EXPECT_EQ (0, factorial (-5));
}
