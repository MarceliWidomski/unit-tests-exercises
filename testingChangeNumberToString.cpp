/*
 * testingChangeNumberToString.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"

TEST (TestingChangeNumberToString, handlesZero){
	EXPECT_EQ(std::string("zero "), changeNumberToString(0));
}
TEST (TestingChangeNumberToString, handlesPositiveNumber){
	EXPECT_EQ(std::string("one "), changeNumberToString(1)); // alternative using c_str function
	EXPECT_EQ(std::string("one two "), changeNumberToString(12));
	EXPECT_EQ(std::string("five four three "), changeNumberToString(543));
	EXPECT_EQ(std::string("nine six five zero one four three two "), changeNumberToString(96501432));
}
TEST (TestingChangeNumberToString, handlesNegativeNumber){
	EXPECT_EQ(std::string("minus three "), changeNumberToString(-3));
	EXPECT_EQ(std::string("minus seven two "), changeNumberToString(-72));
	EXPECT_EQ(std::string("minus eight zero six "), changeNumberToString(-806));
}


