/*
 * testingProductOfDigits.cpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "functions.h"

TEST (TestingProductOfDigits, handlesZero){
	EXPECT_EQ(0, productOfDigits(0));
}
TEST (TestingProductOfDigits, handlesPositive){
	EXPECT_EQ(1, productOfDigits(1));
	EXPECT_EQ(4, productOfDigits(22));
	EXPECT_EQ(6, productOfDigits(123));
}
TEST (TestingProductOfDigits, handlesNegativeWithEvenNumberOfDigits){
	EXPECT_EQ(4, productOfDigits(-22));
	EXPECT_EQ(24, productOfDigits(-1234));
}
TEST (TestingProductOfDigits, handlesNegativeWithOddNumberOfDigits){
	EXPECT_EQ(-2, productOfDigits(-2));
	EXPECT_EQ(-6, productOfDigits(-123));
	EXPECT_EQ(-120, productOfDigits(-12345));
}


