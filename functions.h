/*
 * functions.h
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */


#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <string>

int factorial(int number);
int fibonacci(int n);
int greatestCommonDivisor(int k, int n);
int daysOfMonth(int month, int year = 1);
bool isDateValid(int day, int month, int year);
int numberOfDigits(int number);
int roundExamResult (int examResult);
int productOfDigits (int number);
std::string changeNumberToString (int number);

#endif /* FUNCTIONS_H_ */
